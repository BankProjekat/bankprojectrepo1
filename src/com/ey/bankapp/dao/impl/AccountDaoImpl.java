package com.ey.bankapp.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ey.bankapp.dao.AccountDao;
import com.ey.bankapp.database.impl.DBBrokerImpl;

import models.Account;

public class AccountDaoImpl extends DBBrokerImpl implements AccountDao {

	@Override
	// IR: java code style - blank spaces, rename to getAccounts
	public ArrayList<Account> returnAccounts() {

		makeConnection();

		ArrayList<Account> accounts = new ArrayList<>();
		String upit = "select * from account";

		try {

			// IR: closing connections and prepared statements!?
			ps = conn.prepareStatement(upit);
			ResultSet rs = ps.executeQuery(upit);

			while (rs.next()) {
				accounts.add(new Account(rs.getInt("accountid"), rs.getString("name"), rs.getDouble("balance")));
			}
		} catch (SQLException ex) {
			Logger.getLogger(DBBrokerImpl.class.getName()).log(Level.SEVERE, null, ex);
		}

		closeConnection();

		return accounts;
	}

	@Override
	public double returnBalance(Account fromid) throws SQLException {

		makeConnection();

		PreparedStatement st = conn
				.prepareStatement("select balance from account where accountid=" + fromid.getAccountid());
		ResultSet r = st.executeQuery();
		double dbalance = 0;
		while (r.next()) {
			dbalance = r.getDouble("balance");
		}

		closeConnection();

		return dbalance;
	}

}
