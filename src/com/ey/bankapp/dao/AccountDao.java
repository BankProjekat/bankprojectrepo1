package com.ey.bankapp.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import models.Account;

public interface AccountDao {
	
	ArrayList<Account>returnAccounts();
	
	double returnBalance(Account fromid) throws SQLException;

}
