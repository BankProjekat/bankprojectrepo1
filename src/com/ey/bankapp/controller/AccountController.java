package com.ey.bankapp.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import com.ey.bankapp.dao.AccountDao;
import com.ey.bankapp.dao.impl.AccountDaoImpl;

import models.Account;

public class AccountController {

	private AccountDao accountDao;

	private static AccountController instanca;

	public AccountController() {

		accountDao = new AccountDaoImpl();
	}

	public static AccountController getInstanca() {
		if (instanca == null) {
			instanca = new AccountController();
		}
		return instanca;
	}

	public ArrayList<Account> returnAccounts() {
		return accountDao.returnAccounts();
	}

	public double vratiBalance(Account fromid) throws SQLException {

		double balance = accountDao.returnBalance(fromid);

		return balance;
	}

}
