/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ey.bankapp.controller;

import models.Account;
import models.Transaction;
import forms.FormForTransaction;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import com.ey.bankapp.dao.AccountDao;
import com.ey.bankapp.dao.impl.AccountDaoImpl;
import com.ey.bankapp.database.DBBroker;
import com.ey.bankapp.database.impl.DBBrokerImpl;


public class Controller {

	// IR: add interface class, add javadoc to public methods
    private static Controller instanca;
    private DBBroker db;
    

    public Controller() {
        db = new DBBrokerImpl();
    }

    public static Controller getInstanca() {
        if (instanca == null) {
            instanca = new Controller();
        }
        return instanca;
    }

   

    public void saveTransaction(Transaction t) {
        FormForTransaction fzt = new FormForTransaction();
        db.makeConnection();

        try {
            db.saveTransaction(t);
            db.commit();
            JOptionPane.showMessageDialog(fzt, "transaction is saved");
        } catch (SQLException ex) {
            db.rollback();
            JOptionPane.showMessageDialog(fzt, "Transaction is not saved: " + ex.getMessage());
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.closeConnection();
    }

    public void updateAccountFrom(Account fromid, double amount) {
        FormForTransaction ft = new FormForTransaction();
        db.makeConnection();

        try {
            db.updateAccountFrom(fromid, amount);
            db.commit();
            JOptionPane.showMessageDialog(ft, "accountfrom is updated");
        } catch (SQLException ex) {
            db.rollback();
            JOptionPane.showMessageDialog(ft, "accountafrom is not updated: " + ex.getMessage());
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.closeConnection();
    }

    public void updateAccountTo(Account toid, double amount) {
        FormForTransaction ft = new FormForTransaction();
        db.makeConnection();

        try {
            db.updateAccountTo(toid, amount);
            db.commit();
            JOptionPane.showMessageDialog(ft, "accountto is updated");
        } catch (SQLException ex) {
            db.rollback();
            JOptionPane.showMessageDialog(ft, "accountato is not updated: " + ex.getMessage());
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.closeConnection();
    }

    

}
