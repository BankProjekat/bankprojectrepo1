package com.ey.bankapp.database;

import java.sql.SQLException;
import java.util.ArrayList;

import models.Account;
import models.Transaction;

public interface DBBroker {
	
	void makeConnection();
	
	void closeConnection();
	
	 void commit();
	 
	 void rollback();
	 
	 
	 void saveTransaction(Transaction t) throws SQLException;
	 
	 void updateAccountFrom(Account fromid,double amount) throws SQLException;
	 
	 void updateAccountTo(Account toid,double amount) throws SQLException;
	 
	 void uploadFromCSVFile(String csvFilePath1,String csvFilePath2);
	 
	 

}
