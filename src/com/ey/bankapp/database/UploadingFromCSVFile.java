/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ey.bankapp.database;

import forms.FormForTransaction;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ey.bankapp.database.impl.DBBrokerImpl;

/**
 *
 * @author KB917CM
 */
public class UploadingFromCSVFile {

    public static DBBroker db = new DBBrokerImpl();
    public static String csvFilePath1 = "account.csv";
    public static String csvFilePath2 = "transaction.csv";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       

        db.uploadFromCSVFile(csvFilePath1, csvFilePath2);

       
        
        FormForTransaction fm=new FormForTransaction();
        fm.setVisible(true);
        

    }
}
