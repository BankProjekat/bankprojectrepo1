/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ey.bankapp.database.impl;

import models.Account;
import models.Transaction;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ey.bankapp.database.DBBroker;

/**
 *
 * @author KB917CM
 */
public class DBBrokerImpl implements DBBroker {

	protected Connection conn;
	protected PreparedStatement ps;

	// IR: instantiate logger as private field
	public void makeConnection() {

		try {

			// IR: move connection parameters to property file

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/projekat", "root", "");
			conn.setAutoCommit(false);

		} catch (SQLException e) {

			// IR: no need for system out print ln, error message may be added to logger
			System.out.println("Mistake during connection to database" + e);
			Logger.getLogger(DBBrokerImpl.class.getName()).log(Level.SEVERE, null, e);
		}
	}

	public void closeConnection() {
		try {
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(DBBrokerImpl.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void commit() {
		try {
			conn.commit();
		} catch (SQLException ex) {
			Logger.getLogger(DBBrokerImpl.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void rollback() {
		try {
			conn.rollback();
		} catch (SQLException ex) {
			Logger.getLogger(DBBrokerImpl.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	// IR: java code style - blank spaces, rename to getAccounts
	public ArrayList<Account> returnAccounts() {
		ArrayList<Account> accounts = new ArrayList<>();
		String upit = "select * from account";

		try {

			// IR: closing connections and prepared statements!?
			ps = conn.prepareStatement(upit);
			ResultSet rs = ps.executeQuery(upit);

			while (rs.next()) {
				accounts.add(new Account(rs.getInt("accountid"), rs.getString("name"), rs.getDouble("balance")));
			}
		} catch (SQLException ex) {
			Logger.getLogger(DBBrokerImpl.class.getName()).log(Level.SEVERE, null, ex);
		}

		return accounts;
	}

	public void saveTransaction(Transaction t) throws SQLException {
		String upit = "insert into transaction values(?,?,?,?,?)";
		ps = conn.prepareStatement(upit);

		ps.setInt(1, t.getTransactionid());
		ps.setInt(2, t.getAccount().getAccountid());
		ps.setInt(3, t.getAccount().getAccountid());
		ps.setDouble(4, t.getAmount());
		ps.setString(5, t.getStatus());

		ps.execute();
	}

	// IR: code style, new lines, tabs, formatting
	public void updateAccountFrom(Account fromid, double amount) throws SQLException {
		String upit = "update account set balance=balance-" + amount + " where accountid=" + fromid.getAccountid();

		ps = conn.prepareStatement(upit);

		ps.executeUpdate(upit);
	}

	public void updateAccountTo(Account toid, double amount) throws SQLException {
		String upit = "update account set balance=balance+" + amount + " where accountid=" + toid.getAccountid();
		ps = conn.prepareStatement(upit);
		ps.executeUpdate(upit);
	}

	public double returnBalance(Account fromid) throws SQLException {
		PreparedStatement st = conn
				.prepareStatement("select balance from account where accountid=" + fromid.getAccountid());
		ResultSet r = st.executeQuery();
		double dbalance = 0;
		while (r.next()) {
			dbalance = r.getDouble("balance");
		}

		return dbalance;
	}

	// IR: method too long, create smaller private methods
	public void uploadFromCSVFile(String csvFilePath1, String csvFilePath2) {

		makeConnection();

		try {

			int batchSize = 40;

			uploadAccounts(csvFilePath1, batchSize);

			uploadTransactions(csvFilePath2, batchSize);

			conn.commit();
			conn.close();

		} catch (IOException ex) {
			System.err.println(ex);
		} catch (SQLException ex) {
			ex.printStackTrace();

			try {
				conn.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		closeConnection();

	}

	private void uploadTransactions(String csvFilePath2, int batchSize)
			throws SQLException, FileNotFoundException, IOException {
		PreparedStatement statement;
		BufferedReader lineReader;
		String lineText;
		int count;
		String sql1 = "INSERT INTO transaction(fromID,toID,amount,status) VALUES (?,?,?,?)";
		statement = conn.prepareStatement(sql1);

		lineReader = new BufferedReader(new FileReader(csvFilePath2));
		lineText = null;

		count = 0;

		while ((lineText = lineReader.readLine()) != null) {
			String[] data = lineText.split(";");
			String fromid = data[0];
			String toid = data[1];
			String amount = data[2];

			Integer ifromID = Integer.parseInt(fromid);
			Integer itoID = Integer.parseInt(toid);
			Double damount = Double.parseDouble(amount);
			statement.setInt(1, ifromID);
			statement.setInt(2, itoID);
			statement.setDouble(3, damount);

			// IR: use set parameter in all prepared statements

			PreparedStatement st = conn.prepareStatement("select balance from account where accountid=" + ifromID);
			ResultSet r = st.executeQuery();

			double dbalance = 0;
			while (r.next()) {
				dbalance = r.getDouble("balance");
			}

			if (dbalance - damount > 0) {

				String sql3 = "update account set balance=balance+" + damount + " where accountid=" + itoID;
				Statement s = conn.createStatement();
				s.executeUpdate(sql3);

				String sql4 = "update account set balance=balance-" + damount + " where accountid=" + ifromID;
				s = conn.createStatement();
				s.executeUpdate(sql4);
				statement.setInt(4, 1);
			} else {

				statement.setInt(4, 0);

			}

			statement.addBatch();

			if (count % batchSize == 0) {
				statement.executeBatch();
			}
		}

		lineReader.close();

		// execute the remaining queries
		statement.executeBatch();
	}

	private void uploadAccounts(String csvFilePath1, int batchSize)
			throws SQLException, FileNotFoundException, IOException {

		String sql = "INSERT INTO account(name,balance) VALUES (?,?)";
		PreparedStatement statement = conn.prepareStatement(sql);

		BufferedReader lineReader = new BufferedReader(new FileReader(csvFilePath1));
		String lineText = null;

		int count = 0;

		while ((lineText = lineReader.readLine()) != null) {

			String[] data = lineText.split(";");
			String name = data[0];
			String balance = data[1];

			addLineToBatch(statement, name, balance);

			if (count % batchSize == 0) {
				statement.executeBatch();
			}
		}

		lineReader.close();

		// execute the remaining queries
		statement.executeBatch();
	}

	private void addLineToBatch(PreparedStatement statement, String name, String balance) throws SQLException {
		statement.setString(1, name);
		Double fbalance = Double.parseDouble(balance);
		statement.setDouble(2, fbalance);
		statement.addBatch();
	}
}
