/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Objects;

/**
 *
 * @author KB917CM
 */
public class Account {
    private int accountid;
    private String name;
    private double balance;

    public Account() {
    }

    public Account(int accountid, String name, double balance) {
        this.accountid = accountid;
        this.name = name;
        this.balance = balance;
    }

    public int getAccountid() {
        return accountid;
    }

    public void setAccountid(int accountid) {
        this.accountid = accountid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

   

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Account other = (Account) obj;
        if (this.accountid != other.accountid) {
            return false;
        }
        return true;
    }

    
    
    @Override
    public String toString() {
        return name+" (id:"+accountid+")";
    }
 
    
    
    
    
}
