/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author KB917CM
 */
public class Transaction {

    private int transactionid;
    private Account fromID;
    private Account toID;
    private double amount;
    private String status;

    public Transaction() {
    }

    public Transaction(int transactionid, Account fromID, Account toID, double amount, String status) {
        this.transactionid = transactionid;
        this.fromID = fromID;
        this.toID = toID;
        this.amount = amount;
        this.status = status;
    }

    public int getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(int transactionid) {
        this.transactionid = transactionid;
    }

    public Account getFromID() {
        return fromID;
    }

    public void setFromID(Account fromID) {
        this.fromID = fromID;
    }

    public Account getToID() {
        return toID;
    }

    public Account getAccount() {
        return toID;
    }

    public void setAccount(Account account) {
        this.toID = toID;
    }

    public void setToID(Account toID) {
        this.toID = toID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transaction other = (Transaction) obj;
        if (this.transactionid != other.transactionid) {
            return false;
        }
        return true;
    }

}
